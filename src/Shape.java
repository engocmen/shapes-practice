/**
 *
 * FIRST, COMPLETE THIS INTERFACE
 * THEN, COMPLETE Rectangle, Square, and Circle classes
 * LASTLY, COMPLETE TestShapes class
 */

public interface Shape {

    //Create an abstract method called as area and returns a double



    default double area(){
        return 0;
    }


    //Create an abstract method called as perimeter and returns a double
    default double perimeter(){
        return 0;
    }


}
